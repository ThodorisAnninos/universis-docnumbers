import {DataError} from "@themost/common";
/**
 * @param {DataEventArgs} event
 */
async function afterSaveAsync(event) {
    const context  = event.model.context;
    // get previous state of published attribute
    let previousPublished = false;
    let previouslySigned = false;
    let eventLogTitle;
    if (event.state === 2) {
        if (event.previous == null) {
            throw new DataError('E_STATE', 'Current item previous state cannot be found or is inaccessible.', event.model.name);
        }
        previousPublished = event.previous.published;
        previouslySigned = event.previous.signed;
        if (Object.prototype.hasOwnProperty.call(event.target, 'url') && event.previous.url!==event.target.url) {
            // document is re-issued
            eventLogTitle = context.__('Reissue document');
        }
    }
    // get original published attribute
    const published = await event.model.where('id').equal(event.target.id).select('published').silent().value();
    const RequestDocumentActions = context.model('RequestDocumentAction');
    if (RequestDocumentActions == null) {
        return;
    }
    let action = await RequestDocumentActions.where('result').equal(event.target.id).silent().getTypedItem();
    // get student account
    let user = null;
    if (action) {
        user = await context.model('Student').find(action.student).select('user').silent().value();
    }
    if (published === true && previousPublished === false) {
        // search for request document action with result
        // check documentStatus, cancelled document cannot be published
        const documentStatus = await context.model(event.model.name).where('id').equal(event.target.id).select('documentStatus/alternateName as documentStatus').silent().value();
        if (documentStatus==='CancelledDocumentStatus') {
            throw new DataError('E_STATE', 'Current item document status is not allowed for publishing.', event.model.name);
        }
        if (action && action.actionStatus.alternateName === 'CompletedActionStatus') {
            if (user != null) {
                // add message
                const newMessage = await context.model('StudentMessage').silent().save({
                    student: action.student,
                    action: action.id,
                    subject: context.__('CompletedStudentRequestSubject'),
                    body: context.__('AttachmentFileCreated')
                });
                // get typed message
                const message = context.model('StudentMessage').convert(newMessage);
                // insert attachment
                await message.property('attachments').silent().insert(event.target);
                // change owner
                await event.model.silent().save({
                    id : event.target.id,
                    owner: user
                });
            }
            eventLogTitle = context.__('Document published');
        }
    } else if (published === false && previousPublished === true) {
        action = await RequestDocumentActions
            .where('result').equal(event.target.id)
            .equal(event.target.id)
            .expand({
                name: 'messages',
                options: {
                    $expand: 'attachments'
                }
            })
            .silent()
            .getTypedItem();
        eventLogTitle = context.__('Cancel document publishing');
        if (action && action.actionStatus.alternateName === 'CompletedActionStatus') {
            // find message
            let messageAttachment;
            let actionMessage;
            for (let i = 0; i < action.messages.length; i++) {
                actionMessage = action.messages[i];
                messageAttachment = actionMessage.attachments.find( (item) => {
                   return item.id === event.target.id;
                });
                if (messageAttachment) {
                    break;
                }
            }
            // if attachment found
            if (messageAttachment) {
                const message = context.model('StudentMessage').convert(actionMessage);
                // remove attachment
                await message.property('attachments').silent().remove(event.target);
                // remove message
                await context.model('StudentMessage').remove(message);
                // change owner
                if (user) {
                    await event.model.silent().save({
                        id : event.target.id,
                        owner: null
                    });
                }
            }
        }
    }
    if (previouslySigned === false) {
        // check if document is signed
        const signed = await event.model.where('id').equal(event.target.id).select('signed').silent().value();
        if (signed === true) {
            eventLogTitle = context.__('Document digitally signed');
        }
    }
    if (action && eventLogTitle) {
        // add to actionEventLog
        const log = {
            eventSource: action.additionalType,
            title: eventLogTitle,
            action,
            eventType: 1,
            username: (context.interactiveUser && context.interactiveUser.name) || (context.user && context.user.name)
        };
        await context.model('ActionEventLog').silent().save(log);
    }
}
/**
 * @param {DataEventArgs} event
 * @param {Function} callback
 */
export function afterSave(event, callback) {
    return afterSaveAsync(event).then(() => {
        return callback();
    }).catch((err) => {
        return callback(err);
    });
}
