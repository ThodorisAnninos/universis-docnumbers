import {ApplicationService, Args, TraceUtils} from '@themost/common';
import {DefaultEncryptionStrategy, EncryptionStrategy} from "@themost/web";
import {resolve} from 'url';
import {documentNumberVerifier} from "./documentVerifierRouter";
import {sampleSize} from 'lodash';

/**
 * @param {Router} parent
 * @param {Router} before
 * @param {Router} insert
 */
function insertRouterBefore(parent, before, insert) {
    const beforeIndex = parent.stack.findIndex( (item) => {
        return item === before;
    });
    if (beforeIndex < 0) {
        throw new Error('Target router cannot be found in parent stack.');
    }
    const findIndex = parent.stack.findIndex( (item) => {
        return item === insert;
    });
    if (findIndex < 0) {
        throw new Error('Router to be inserted cannot be found in parent stack.');
    }
    // remove last router
    parent.stack.splice(findIndex, 1);
    // move up
    parent.stack.splice(beforeIndex, 0, insert);
}

// eslint-disable-next-line no-unused-vars
/**
 * @abstract
 * An application service for validating document series items
 */
class DocumentNumberVerifierService extends ApplicationService {
    constructor(app) {
        super(app);
        // validate origin
        const origin = app.getConfiguration().getSourceAt('settings/universis/docnumbers/origin');
        if (origin == null) {
            throw new Error('Invalid configuration. universis/docnumbers#origin configuration setting is required by DocumentNumberValidator.');
        }
        // if encryption strategy is missing
        if (app.hasStrategy(EncryptionStrategy) === false) {
            // add encryption strategy
            app.useStrategy(EncryptionStrategy, DefaultEncryptionStrategy);
        }
        if (app.container) {
            // subscribe and wait for container
            app.container.subscribe((container) => {
                if (container) {
                    // use router
                    container.use('/documents', documentNumberVerifier(app));
                    // get container router
                    const router = container._router;
                    // find before position
                    const before = router.stack.find((item) => {
                        return item.name === 'dataContextMiddleware';
                    });
                    if (before == null) {
                        // do nothing
                        return;
                    }
                    const insert = router.stack[router.stack.length - 1];
                    // re-index router
                    insertRouterBefore(router, before, insert);
                }
            });
        } else {
            // container is missing
            TraceUtils.warn('DocumentNumberVerifierService', 'Application container is missing. Document verifier service endpoint will be unavailable.');
        }        
    }

    /**
     * @param {*} documentSeries
     * @param {*} documentNumber
     * @param {*=} timestamp
     */
    generateURI(documentSeries, documentNumber, timestamp) {
        //validate parameters
        Args.notEmpty(documentSeries, 'documentSeries');
        Args.notEmpty(documentNumber, 'documentNumber');
        // get origin
        const origin = this.getApplication().getConfiguration().getSourceAt('settings/universis/docnumbers/origin');
        if (origin == null) {
            throw new Error('Invalid configuration. Document validator origin is missing');
        }
        /**
         * get encryption service
         * @type {EncryptionStrategy}
         */
        const encryptionStrategy = this.getApplication().getStrategy(EncryptionStrategy);
        const code = encryptionStrategy.encrypt(JSON.stringify({
            documentSeries,
            documentNumber,
            timestamp
        }));
        // generate URI
        return resolve(origin, `documents/verify?code=${code}`);
    }

    getVerifierURI() {
        // get origin
        const origin = this.getApplication().getConfiguration().getSourceAt('settings/universis/docnumbers/origin');
        if (origin == null) {
            throw new Error('Invalid configuration. Document validator origin is missing');
        }
        return resolve(origin, `documents/verify/form`);
    }

    /**
     * @param {DataContext} context 
     * @returns 
     */
    // eslint-disable-next-line no-unused-vars
    generateShortDocumentCode(context) {
        return sampleSize('ABCDEFGHIKLMNOPQRTUVWXYZ0123456789', 20).join('');
    }

}

export {
    DocumentNumberVerifierService
}
