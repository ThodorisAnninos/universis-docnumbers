import { HttpError, HttpConflictError, HttpBadRequestError, HttpNotFoundError, HttpServerError, TraceUtils, HttpTokenExpiredError} from "@themost/common";
import {EncryptionStrategy} from "@themost/web";
import { PatternValidator } from '@themost/data';
import DocumentNumberSeriesItem from "./models/DocumentNumberSeriesItem";
// eslint-disable-next-line no-unused-vars
const {ExpressDataApplication} = require('@themost/express');
import {Router} from 'express';
import path from 'path';
import util from 'util';
import moment from 'moment';
import rateLimit from 'express-rate-limit'

const el = require('../locales/el.json');
const en = require('../locales/en.json');
/**
 *
 * @param {ExpressDataApplication} app
 * @returns {*}
 */
function documentNumberVerifier(app) {

    const translateService = app.getService(function TranslateService() {
    });
    translateService.setTranslation('el', el);
    translateService.setTranslation('en', en);

    function getShortDocumentCodeDuration(req) {
        return req.context.application.getConfiguration().getSourceAt('settings/universis/docnumbers/shortDocumentCodeDuration') || 'P1M';
    }
    function getShortDocumentCodeDurationText(req) {
        return moment.duration(getShortDocumentCodeDuration(req)).locale(req.locale).humanize();
    }

    const router = Router();
    // use this handler to create a router context
    router.use((req, res, next) => {
        // create router context
        const newContext = app.createContext();
        /**
         * try to find if request has already a data context
         * @type {ExpressDataContext|*}
         */
        const interactiveContext = req.context;
        // finalize already assigned context
        if (interactiveContext) {
            if (typeof interactiveContext.finalize === 'function') {
                // finalize interactive context
                return interactiveContext.finalize(() => {
                    // and assign new context
                    Object.defineProperty(req, 'context', {
                        enumerable: false,
                        configurable: true,
                        get: () => {
                            return newContext
                        }
                    });
                    // exit handler
                    return next();
                });
            }
        }
        // otherwise assign context
        Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            get: () => {
                return newContext
            }
        });
        // and exit handler
        return next();
    });
    router.use((req, res, next) => {
        // set context locale from request
        req.context.locale  = req.locale;
        // set translate
        const translateService = req.context.application.getStrategy(function TranslateService() {});
        if (translateService) {
            const TranslateServiceClass = translateService.constructor;
            const translateServiceContext = new TranslateServiceClass(req.context.application);
            translateServiceContext.defaultLocale = req.locale;
            Object.defineProperty(req.context, 'translateService', {
                enumerable: false,
                configurable: true,
                value: translateServiceContext
            });
        }
        req.context.translate = function () {
            if (this.translateService == null) {
                return arguments[0];
            }
            return this.translateService.translate.apply(this.translateService, Array.from(arguments));
        };
        return next();
    });
    // use this handler to finalize router context
    // important note: context finalization is very important in order
    // to close and finalize database connections, cache connections etc.
    router.use((req, res, next) => {
        req.on('end', () => {
            //on end
            if (req.context) {
                //finalize data context
                return req.context.finalize( () => {
                    //
                });
            }
        });
        return next();
    });

    router.get('/verify/form', (req, res) => {
        return res.status(200).render(path.resolve(__dirname, 'views/form'), {
            ok: true,
            model: {
                shortDocumentCode: ''
            },
            html: {
                context: req.context,
                duration: getShortDocumentCodeDurationText(req)
            }
        });
    });

    // get rate limit options from application configuration
    const rateLimitOptions = Object.assign({
        windowMs: 5 * 60 * 1000, // 5 minutes
        max: 20, // 20 requests
        standardHeaders: true,
        legacyHeaders: false
    }, app.getConfiguration().getSourceAt('settings/universis/docnumbers/rateLimit'));
    router.post('/verify/form', 
    rateLimit(Object.assign(rateLimitOptions, {
        keyGenerator: (req) => {
            return req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || (req.connection ? req.connection.remoteAddress : req.socket.remoteAddress);
        },
        // eslint-disable-next-line no-unused-vars
        handler: (req, res, next, options) => {
            return res.status(429).render(path.resolve(__dirname, 'views/form'), {
                ok: false,
                html: {
                    context: req.context,
                    values: {
                        minutes: Math.ceil(rateLimitOptions.windowMs/60000)
                    }
                },
                model: {
                    shortDocumentCode: ''
                },
                error: new HttpError(429, 'Too Many Requests')
            });
        }
    })),

    /**
     * @param {Request} req
     * @param {Response} res
     * @param {*} next
     * @returns {Promise<*>}
     */
    async function postDocumentVerify(req, res) {
        let shortDocumentCode;
        try {
            shortDocumentCode = req.body.shortDocumentCode;
            if (shortDocumentCode == null) {
                throw new HttpBadRequestError('Document code is missing.');
            }
            // validate document code
            const shortCode = shortDocumentCode.replace(/-/g, '');
            /**
             * @type {DataField}
             */
            const field = req.context.model(DocumentNumberSeriesItem).getAttribute('shortDocumentCode');
            const pattern = field && field.validation && field.validation.pattern;
            if (pattern) {
                const result = new PatternValidator(pattern).validateSync(shortCode);
                if (result != null) {
                    throw new HttpBadRequestError(result.message);
                }
            }
            /**
             * @type {DocumentNumberSeriesItem}
             */
            const item = await req.context.model(DocumentNumberSeriesItem)
                .where('shortDocumentCode').equal(shortCode)
                .silent().getItem();
            if (item == null) {
                throw new HttpNotFoundError('Document cannot be found');
            }
            if (!item.published) {
                throw new HttpError(417, req.context.translate('DocumentVerifierForm.E417'));
            }
            // todo: validate date published
            const service = req.context.getApplication().getService(function DocumentNumberVerifierService() {});
            if (service == null) {
                throw new HttpServerError('Document verifier service cannot be found');
            }

            // get short document duration
            const duration = getShortDocumentCodeDuration(req)
            const validFrom = item.datePublished || item.dateCreated;
            if (validFrom instanceof Date) {
                const validThrough = moment(validFrom).add(moment.duration(duration)).toDate();
                if (validThrough <= new Date()) {
                    throw new HttpTokenExpiredError('Document code has been expired based on system configuration.');
                }
            } else {
                throw new HttpConflictError('Document timestamp must be a valid date');
            }

            const generatedURI = service.generateURI(item.parentDocumentSeries, item.documentNumber, new Date());
            return res.redirect(301, generatedURI);
        } catch (err) {
            TraceUtils.error(err);
            return res.status(err.statusCode || 500).render(path.resolve(__dirname, 'views/form'), {
                ok: false,
                html: {
                    context: req.context,
                    duration: getShortDocumentCodeDurationText(req)
                },
                model: {
                    shortDocumentCode: shortDocumentCode || ''
                },
                error: err
            });
        }
        
    });

    router.get('/verify',
        rateLimit(Object.assign(rateLimitOptions, {
            keyGenerator: (req) => {
                return req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || (req.connection ? req.connection.remoteAddress : req.socket.remoteAddress);
            },
            // eslint-disable-next-line no-unused-vars
            handler: (req, res, next, options) => {
                return res.status(429).render(path.resolve(__dirname, 'views/verify'), {
                    ok: false,
                    html: {
                        context: req.context
                    },
                    error: new HttpError(429, req.context.translate('DocumentVerifierForm.E429', {
                        minutes:  Math.ceil(rateLimitOptions.windowMs/60000)
                    }))
                });
            }
        })),
        /**
         * @param {Request} req
         * @param {Response} res
         * @param {*} next
         * @returns {Promise<*>}
         */
        async function documentVerify(req, res, next) {
        if (req.query.code == null) {
            // render with 400 Bad Request
            return res.status(400).render(path.resolve(__dirname, 'views/verify'), {
                ok: false,
                html: {
                    context: req.context
                },
                error: new HttpBadRequestError('Document code is missing.')
            });
        }
        // decode param
        let codeParams;
        try {
            const codeParamsString = req.context.getApplication().getService(EncryptionStrategy).decrypt(req.query.code);
            codeParams = JSON.parse(codeParamsString);
        } catch (err) {
            TraceUtils.error('(DocumentNumberValidator) An error occurred while decrypting document code.');
            TraceUtils.error(err);
            // render with 400 Bad Request
            return res.status(400).render(path.resolve(__dirname, 'views/verify'), {
                ok: false,
                html: {
                    context: req.context
                },
                error: new HttpBadRequestError('Bad document code format.')
            });
        }
        // search for document
        const item = await req.context.model(DocumentNumberSeriesItem)
            .where('parentDocumentSeries').equal(codeParams.documentSeries)
            .and('documentNumber').equal(codeParams.documentNumber)
            .silent()
            .getItem();
        if (item == null) {
            // render with 404 Not Found
            return res.status(404).render(path.resolve(__dirname, 'views/verify'), {
                ok: false,
                html: {
                    context: req.context
                },
                error: new HttpNotFoundError()
            });
        }
        if (!item.published) {
            return res.status(417).render(path.resolve(__dirname, 'views/verify'), {
                ok: false,
                html: {
                    context: req.context
                },
                error: new HttpError(417, req.context.translate('DocumentVerifierForm.E417'))
            });
        }
        // get content service
        // noinspection JSCheckFunctionSignatures
        const service =  req.context.application.getService(function PrivateContentService() {
        });
        if (service == null) {
            // render with 400 Bad Request
            return res.status(400).render(path.resolve(__dirname, 'views/verify'), {
                ok: false,
                html: {
                    context: req.context
                },
                error: new HttpServerError('Content service cannot be found or is inaccessible.')
            });
        }
        // get physical path
        const resolvePhysicalPath = util.promisify(service.resolvePhysicalPath).bind(service);
        const sendFile = util.promisify(res.sendFile).bind(res);
        // send file
        try {
            //get physical file path
            const physicalPath = await resolvePhysicalPath(req.context, item);
            // send file
            res.contentType(item.contentType);
            await sendFile(physicalPath);
        }
        catch (err) {
            // if resolved file cannot be found in the specified path
            if (err.code === 'ENOENT') {
                TraceUtils.error(`An error occurred while trying to resolve physical path for ${item.name}.`);
                TraceUtils.error(err);
                // throw not found error
                return res.status(400).render(path.resolve(__dirname, 'views/verify'), {
                    ok: false,
                    html: {
                        context: req.context
                    },
                    error: new HttpNotFoundError('Not Found')
                });
            }
            // otherwise throw error
            return next(err);
        }

    });
    return router;
}

export {
    documentNumberVerifier
}
