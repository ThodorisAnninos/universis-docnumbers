import {EdmMapping, EdmType, DataObject} from '@themost/data';
import DocumentNumberSeries from './DocumentNumberSeries';

/**
 * @class
 */
declare class InstituteDocumentNumberSeries extends DocumentNumberSeries {

}

export default InstituteDocumentNumberSeries;